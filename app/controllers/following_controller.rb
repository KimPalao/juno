class FollowingController < ApplicationController

    respond_to :json

    def follow
        existing_following = Following.where(followee_id: following_params[:user_id], follower_id: current_user.id).first
        puts existing_following.inspect
        if existing_following.nil?
            following = Following.new({:followee_id => following_params[:user_id], :follower_id => current_user.id})
            if following.save
                render :json => {}, status: 201
            else
                render :json => {:message => 'There was an error'}, status: 500
            end
        else
            existing_following.destroy
            render :json => {}, status: 200
        end
    end

    def following_params
        params.permit(:user_id)
    end


end
