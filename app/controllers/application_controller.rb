class ApplicationController < ActionController::Base

    def get_user_data
        @user = User.find(params[:user_id])
        return render :json => @user.to_json(:methods => [:profile_pic_url])
    end

    private
    def get_user_params
        params.permit(:user_id)
    end


end
