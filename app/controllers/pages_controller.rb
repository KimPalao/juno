class PagesController < ApplicationController
  def home
    @post = Post.new
  end

  def profile
    puts "Hello #{params[:username]}"
    @profile_user = User.where(username: params[:username]).first
  end

  def edit_profile
  end

  def explore
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @post = Post.find(params[:id])
  end

end
