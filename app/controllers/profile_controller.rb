class ProfileController < ApplicationController

    respond_to :json

    def edit
        puts "Hello!"
        begin
            current_user.fullname = params[:fullname]
            if params[:profile_picture]
                current_user.profile_picture.attach(params[:profile_picture])
            end
            current_user.save
            render :json => {  }, status: 200
        rescue => exception
            puts exception
            puts "There was an error"
            render :json => { }, status: 503
        end
    end


    def profile_params
        params.permit(:fullname, :profile_picture)
    end


end
