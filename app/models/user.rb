class User < ApplicationRecord
  include Rails.application.routes.url_helpers
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :email, uniqueness: { case_sensitive: false }, presence: false, default: nil, allow_nil: true
  validates :username, uniqueness: { case_sensitive: false }, presence: true

  def profile_pic_url
    if self.profile_picture.attached?
      rails_blob_path(self.profile_picture, only_path: true)
    else
      ''
    end
  end

  attr_writer :login

  def login
    @login || self.username || self.email
  end

  def email_required?
    false
  end


  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    elsif conditions.has_key?(:username) || conditions.has_key?(:email)
      where(conditions.to_h).first
    end
  end

  has_many :followings

  has_many :follower_relationships, foreign_key: :followee_id, class_name: :Following
  has_many :followers, :through => :follower_relationships, source: :follower
  
  
  has_many :followee_relationships, foreign_key: :follower_id, class_name: :Following
  has_many :followees, :through => :followee_relationships, source: :followee

  has_many :posts, foreign_key: :poster_id, class_name: :Post
  has_one_attached :profile_picture

end

