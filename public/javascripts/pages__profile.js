(function (d) {

    const posts_column = d.querySelector('#posts');

    /**
     *  
     * @param {string} element 
     * @param {Array<string>} classes 
     * @param {Object} attributes
     * @param {(string|HTMLElement)} content
     * @returns {HTMLElement}
     */

    async function get_posts() {
        const response = await axios.get(`/posts/user/${location.pathname.split("/").pop()}`);
        if (response.status === 200) {
            for (let post of response.data) {
                const p = make_post(post);
                posts_column.appendChild(p);
            }
        }

    }

    get_posts();

    const form = d.querySelector('form');
    const user_id = d.querySelector('input[name="user_id"]');
    const authenticity_token_input = d.querySelector('input[name="authenticity_token"]');

    form.addEventListener('submit', async event => {
        event.stopImmediatePropagation();
        event.preventDefault();
        try {
            const response = await axios.post(event.target.action, {
                user_id: user_id.value,
                authenticity_token: authenticity_token_input.value
            });
            if (response.status === 201) {
                console.log('Followed');
                follow_button.innerText = 'Unfollow';
            } else if (response.status === 200) {
                console.log('Unfollowed');
                follow_button.innerText = 'Follow';
            } else {

            }
        } catch (error) {
            console.log(error);
        }
    });


})(document);