/**
     *  
     * @param {string} element 
     * @param {Array<string>} classes 
     * @param {Object} attributes
     * @param {(string|HTMLElement)} content
     * @returns {HTMLElement}
*/
function createElement(element, classes, attributes, content) {
    const el = document.createElement(element);

    if (classes)
        classes.map(cls => el.classList.add(cls));

    if (attributes) {
        for (let attribute in attributes) {
            el.setAttribute(attribute, attributes[attribute]);
        }
    }

    if (content) {
        if (typeof (content) === 'string') {
            el.innerText = content;
        } else if (typeof (content) === HTMLElement) {
            el.appendChild(content);
        }
    }

    return el;
}

const posters = [];

function make_post(post) {
    const box = createElement('div', ['box']);
    const media = createElement('article', ['media']);


    const media_left = createElement('div', ['media-left']);
    const figure = createElement('figure', ['image', 'is-64x64']);
    const img = createElement('img', ['profile-image'], { src: '', alt: '' });
    figure.appendChild(img);
    media_left.appendChild(figure);

    const media_content = createElement('div', ['media-content']);
    const content = createElement('div', ['content']);

    const profile_link_holder = createElement('a', [], { href: '#', target: '_blank' });
    const name_holder = createElement('strong', [], {}, '');
    const username_holder = createElement('small', [], {}, '@');

    (async function () {
        if (!(post.poster_id in posters)) {
            try {
                const response = await axios(`/user-data/${post.poster_id}`);
                const poster = response.data;
                if (response.status === 200) {
                    posters[post.poster_id] = poster;
                }
            } catch (error) {

            }
        }

        img.setAttribute('src', posters[post.poster_id].profile_pic_url);
        img.setAttribute('alt', posters[post.poster_id].username);
        profile_link_holder.setAttribute('href', `/u/${posters[post.poster_id].username}`);
        name_holder.innerText = posters[post.poster_id].fullname;
        username_holder.innerText += posters[post.poster_id].username;


    })();

    profile_link_holder.appendChild(name_holder);
    profile_link_holder.appendChild(document.createTextNode(' '));
    profile_link_holder.appendChild(username_holder);

    content.appendChild(profile_link_holder);
    content.appendChild(createElement('br'));
    const p = document.createTextNode(post.content);
    content.appendChild(p);
    content.appendChild(createElement('br'));

    const date_parts = post.created_at.split('T');
    const time = date_parts.pop();
    const date = date_parts.pop();
    content.appendChild(createElement(
        'small',
        [],
        {},
        `${date} ${time.substring(0, time.indexOf('.'))}`
    ));

    // const nav = createElement('na  v', ['level', 'is-mobile']);
    // const level_left = createElement('div', ['level-left']);
    // const level_item = createElement('a', ['level-item'], { 'aria-label': 'reply' });
    // const icon_container = createElement('span', ['icon', 'is-small']);
    // const icon = createElement('i', ['fas', 'fa-reply'], { 'aria-hidden': 'true' });
    // icon_container.appendChild(icon);
    // level_item.appendChild(icon_container);
    // level_left.appendChild(level_item);
    // nav.appendChild(level_left);

    media_content.appendChild(content);
    // media_content.appendChild(nav);

    media.appendChild(media_left);
    media.appendChild(media_content);

    box.appendChild(media);
    return box;
}