(function (d) {

    const form = d.querySelector('form');
    const authenticity_token = d.querySelector('input[name="authenticity_token"]');
    const fullname = d.querySelector('input#fullname');
    const profile_picture = d.querySelector('input#profile_picture');
    const file_name_container = d.querySelector('.file-name');
    profile_picture.addEventListener('change', event => {
        if (event.target.files.length > 0) {
            file_name_container.innerText = event.target.files[0].name;
        } else {
            file_name_container.innerText = '';
        }
    });

    form.addEventListener('submit', async (event) => {
        event.stopImmediatePropagation();
        event.preventDefault();
        console.log(profile_picture.files);
        const form_data = new FormData();
        form_data.append('authenticity_token', authenticity_token.value);
        form_data.append('fullname', fullname.value);
        if (profile_picture.files.length > 0)
            form_data.append('profile_picture', profile_picture.files[0], profile_picture.files[0].filename);
        const response = await axios.post('', form_data, {
            headers: { 'content-type': 'multipart/form-data' }
        });
        if (response.status === 200) {
            window.location.pathname = '';
        }

    });

})(document);