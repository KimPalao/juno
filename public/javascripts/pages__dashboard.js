(function (d) {

    const posts_column = d.querySelector('#posts');


    const posts = [
        {
            user: {
                fullname: 'caitlynn j.',
                username: 'cafeaucait',
                image: 'https://bulma.io/images/placeholders/128x128.png',
            },
            content: 'Hello World!',
        },
        {
            user: {
                fullname: 'told',
                username: 'cafeaucait2',
                image: 'https://bulma.io/images/placeholders/128x128.png',
            },
            content: 'me',
        },
        {
            user: {
                fullname: 'gonna',
                username: 'roll',
                image: 'https://bulma.io/images/placeholders/128x128.png',
            },
            content: 'me',
        },
    ];

    async function get_posts() {
        const response = await axios.get(`/feed`);
        for (let post of response.data) {
            const p = make_post(post);
            posts_column.appendChild(p);
        }
    }

    get_posts();



    const new_post_form = d.querySelector('form');
    const content_input = d.querySelector('textarea#content');
    const authenticity_token_input = d.querySelector('input[name="authenticity_token"]');
    new_post_form.addEventListener('submit', event => {
        event.preventDefault();
        event.stopImmediatePropagation();

        if (content_input.value.trim().length === 0) return;
        axios.post(event.target.action, {
            content: content_input.value,
            authenticity_token: authenticity_token_input.value
        }).then(response => {
            if (response.status === 201) {
                posts_column.insertBefore(make_post({
                    content: response.data.content,
                    poster_id: response.data.poster_id,
                    created_at: response.data.created_at,
                }), posts_column.firstChild);
                content_input.value = '';
            } else {

            }
        });


    });

})(document);