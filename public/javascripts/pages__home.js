(function (d) {
    const form = d.querySelector('form');
    const username_input = d.querySelector('input#username');
    const password_input = d.querySelector('input#password');
    const password_conf_input = d.querySelector('input#password_conf');
    const authenticity_token_input = d.querySelector('input[name="authenticity_token"]');
    const submit_button = d.querySelector('#registration_submit_button');
    form.addEventListener('submit', event => {
        submit_button.setAttribute('disabled', '');
        console.log('submitting...');
        event.preventDefault();
        event.stopImmediatePropagation();
        if (password_input.value != password_conf_input.value) {
            console.log(`${password_input.value} != ${password_conf_input.value}`);
            return;
        }
        axios.post(event.target.action, {
            username: username_input.value,
            password: password_input.value,
            password_comfirmation: password_conf_input.value,
            authenticity_token: authenticity_token_input.value
        }).then(response => {
            console.log(response);
            if (response.status === 201) {
                // window.location.href = `${location.origin}/login`;
                window.location.reload(true);
            }
        }).catch(error => {
            console.log(error);
        }).finally(() => {
            submit_button.removeAttribute('disabled');
        });
    });
})(document);