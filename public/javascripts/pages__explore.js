(function (d) {

    const posts_column = d.querySelector('#posts');

    /**
     *  
     * @param {string} element 
     * @param {Array<string>} classes 
     * @param {Object} attributes
     * @param {(string|HTMLElement)} content
     * @returns {HTMLElement}
     */

    async function get_posts() {
        const response = await axios.get('/posts');
        if (response.status === 200) {
            for (let post of response.data) {
                const p = make_post(post);
                posts_column.appendChild(p);
            }
        }

    }

    get_posts();
})(document);