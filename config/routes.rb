Rails.application.routes.draw do
  get 'posts/user/:username' => 'posts#posts_by_user'
  resources :posts
  get 'feed' => 'posts#get_feed'
  root to: 'pages#home'
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
  }

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get 'login' => 'pages#login'
  get 'u/:username' => 'pages#profile'
  get 'profile/edit' => 'pages#edit_profile'
  get 'user-data/:user_id' => 'application#get_user_data'
  get 'explore' => 'pages#explore'
  post 'profile/edit' => 'profile#edit'

  post 'profile/edit' => 'profile#edit'

  post 'follow' => 'following#follow'

end
